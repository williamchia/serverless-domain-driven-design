#!/usr/bin/env bash
set -e

export AWS_REGION="us-east-2"
export aws_stage="kb-local"

aws cloudformation delete-stack --stack-name sac2019-js-${aws_stage}